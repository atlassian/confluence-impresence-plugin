package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;

public class YahooPresenceTestCase extends AbstractPresenceTestCase
{

    private String targetUser;

    protected void setUp() throws Exception
    {
        super.setUp();
        targetUser = "john.doe@localhost.localdomain";
    }

    protected boolean requiresConfiguration()
    {
        return false;
    }

    protected boolean hasPresenceImageInCurrentPage() throws SAXException
    {
        final String sourceToLookFor = "http://opi.yahoo.com/online?u=" + targetUser + "&m=g&t=0";
        return getDialog().hasElementByXPath("//img[starts-with(@src,'" + sourceToLookFor + "')]");
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=yahoo}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Now, let's see if the page shows the presence of the targeted user */
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTrue(hasPresenceImageInCurrentPage());
        assertLinkPresentWithText(targetUser);
    }

    public void testShowPresenceWithoutId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence Without ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=yahoo|showid=false}"
        );

        assertTrue(pageHelper.create());

        /* Now, let's see if the page shows the presence of the targeted user */
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTrue(hasPresenceImageInCurrentPage());
        assertLinkNotPresentWithText(targetUser);
    }
}
