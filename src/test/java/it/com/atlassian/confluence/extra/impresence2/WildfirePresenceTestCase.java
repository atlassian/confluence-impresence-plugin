package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;

public class WildfirePresenceTestCase extends AbstractPresenceTestCase
{
    private String targetUser;

    protected void setUp() throws Exception
    {
        super.setUp();
        getBandanaHelper("extra.im.server.name.wildfire").delete();
        targetUser = "dchui@chat.atlassian.com";
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=wildfire}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Openfire service before you can use this macro.");
        assertLinkPresentWithText("Configure Openfire service");
    }

    protected boolean hasPresenceImageInCurrentPage() throws SAXException
    {
        final String sourceToLookFor = "http://chat.atlassian.com/plugins/presence/status?jid=" + targetUser;
        return getDialog().hasElementByXPath("//img[starts-with(@src,'" + sourceToLookFor + "')]");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=wildfire}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);
        try
        {
            gotoPage("/");
            gotoPage("/display/" + testSpaceKey);

            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextPresent("An administrator must configure your server's Openfire service before you can use this macro.");
            assertLinkNotPresentWithText("Configure Openfire service");
        }
        finally
        {
            logout();
            loginAsAdmin(); /* So tearDown can be called successfully */
        }
    }

    private void configureWildfireServer(final String serverLocation)
    {
        setWorkingForm("configurewildfireform");
        setTextField("server", serverLocation);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure Openfire service - Confluence");
        assertThat(getElementAttributeByXPath("//input[@name='server']", "value"), is(serverLocation));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=wildfire}"
        );

        assertTrue(pageHelper.create());

        try
        {
            // Configure Jive Wildfire Service requires escalated privileges
            gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());

            clickLinkWithText("Configure Openfire service");

            assertTitleEquals("Configure Openfire service - Confluence");
            assertThat(getElementAttributeByXPath("//input[@name='server']", "value"), isEmptyString());

            configureWildfireServer("chat.atlassian.com");

        	/* Now, let's see if the page shows the presence of the targeted user */
            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextNotPresent("An administrator must configure your server's Openfire service before you can use this macro.");
            assertTrue(hasPresenceImageInCurrentPage());
            assertLinkPresentWithText(targetUser);
        }
        finally
        {
            dropEscalatedPrivileges();
            assertTrue(getBandanaHelper("extra.im.server.name.wildfire").delete());
        }
    }

    public void testShowPresenceWithoutId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence Without ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=wildfire|showid=false}"
        );

        assertTrue(pageHelper.create());

        try
        {
            // Configure Wildfire Service requires escalated privileges
            gotoPageWithEscalatedPrivileges("/admin/plugins/impresence2/wildfire/config.action");
            configureWildfireServer("chat.atlassian.com");

        	/* Now, let's see if the page shows the presence of the targeted user */
            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextNotPresent("An administrator must configure your server's Openfire service before you can use this macro.");
            assertTrue(hasPresenceImageInCurrentPage());
            assertLinkNotPresentWithText(targetUser);
        }
        finally
        {
            dropEscalatedPrivileges();
            assertTrue(getBandanaHelper("extra.im.server.name.wildfire").delete());
        }
    }
}
