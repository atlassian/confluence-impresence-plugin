package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;

public abstract class AbstractPresenceTestCase extends AbstractConfluencePluginWebTestCase
{
    private static final Logger log = LoggerFactory.getLogger(AbstractPresenceTestCase.class);

    protected String testSpaceKey;

    protected String nonAdminUserName;

    protected String nonAdminPassword;

    protected void setUp() throws Exception
    {
        super.setUp();

        if (requiresConfiguration())
        {
            /*
             * We create a non admin user which can be used to test for the ability to configure dummy accounts.
             * Non admin users should not be able to configure the plugin.
             */
            createNonAdminUser();
        }

        testSpaceKey = "tst";
        createSpace(testSpaceKey, "Test", "Test space");
    }

    @Override
    protected void tearDown() throws Exception {
        deleteSpace(testSpaceKey);
        if (requiresConfiguration())
        {
            getUserHelper(nonAdminUserName).delete();
        }
        super.tearDown();
    }

    // TODO move into AbstractConfluencePluginWebTestCase
    public ExtendedSpaceHelper getSpaceHelper()
    {
        return ExtendedSpaceHelper.createSpaceHelper(getConfluenceWebTester());
    }

    public SpacePermissionsHelper getSpacePermissionsHelper()
    {
        return SpacePermissionsHelper.createSpacePermissionsHelper(getConfluenceWebTester());
    }

    private long createSpace(String spaceKey, String title, String description)
    {
        ExtendedSpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(title);
        spaceHelper.setDescription(description);
        assertTrue(spaceHelper.createWithDefaultPermissions());
        SpacePermissionsHelper spacePermissionsHelper = getSpacePermissionsHelper();
        spacePermissionsHelper.setKey(spaceKey);
        assertTrue(spacePermissionsHelper.addPermission("confluence-users", "VIEWSPACE"));

        return spaceHelper.getHomePageId();
    }

    private void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }

    private void createNonAdminUser()
    {
        final UserHelper userHelper = getUserHelper();

        userHelper.setName(nonAdminUserName = "john.doe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress(userHelper.getName() + "@localhost.localdomain");
        userHelper.setGroups(Arrays.asList("confluence-users" ));
        userHelper.setPassword(nonAdminPassword = "john.doe");

        assertTrue(userHelper.create());

    }

    protected boolean hasPresenceImageInCurrentPage() throws SAXException
    {
        final String sourcePrefix = getContextPath() + "/download/resources/confluence.extra.impresence2:im/images/";
        return getDialog().hasElementByXPath("//img[starts-with(@src,'" + sourcePrefix + "')]");
    }

    private String getContextPath()
    {
        return getElementAttributByXPath("//meta[@id='confluence-context-path']", "content");
    }

    protected abstract boolean requiresConfiguration();
}
