package com.atlassian.confluence.extra.impresence2.reporter;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAimPresenceReporter extends AbstractPresenceReporterTest<AIMPresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return AIMPresenceReporter.KEY;
    }

    protected AIMPresenceReporter createPresenceReporter()
    {
        return new AIMPresenceReporter(localeSupport);
    }

    @Test
    public void testGetKey()
    {
        assertEquals("aim", AIMPresenceReporter.KEY);
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.aim.name", presenceReporter.getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.aim.servicehomepage", presenceReporter.getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertEquals(false, presenceReporter.hasConfig());
    }

    @Test
    public void testRequiresConfig()
    {
        assertEquals(false, presenceReporter.requiresConfig());
    }

    @Test
    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(presenceReporter.getPresenceXHTML(null, false).indexOf("presencereporter.aim.error.noscreenname") >= 0);
    }

    @Test
    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a href='aim:GoIM?screenname=" + id + "'><img src='http://api.oscar.aol.com/SOA/key=jo1rkjdnQ-LEjx49/presence/" + id + "' height='16' width='16' style='vertical-align:bottom; margin:0px 1px;' border='0'/></a>"
                        + "&nbsp;<a title=\"AIM " + id + "\" href=\"aim:GoIM?screenname=" + id + "\">" + id + "</a>",
                presenceReporter.getPresenceXHTML(id, true));
    }

    @Test
    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals("<a href='aim:GoIM?screenname=" + id + "'><img src='http://api.oscar.aol.com/SOA/key=jo1rkjdnQ-LEjx49/presence/" + id + "' height='16' width='16' style='vertical-align:bottom; margin:0px 1px;' border='0'/></a>",
                presenceReporter.getPresenceXHTML(id, false));
    }
}