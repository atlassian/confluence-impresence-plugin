package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSameTimePresenceReporter extends AbstractServerPresenceReporterTest<SametimePresenceReporter>
{

    private Map<String, Object> velocityContext;

    @Mock
    private VelocityHelperService velocityHelperService;

    @Before
    public void setUp() throws Exception
    {
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                SERVER_NAME + getPresenceReporterKey()
        )).thenReturn("imaginary-server");

        velocityContext = new HashMap<String, Object>();
    }

    protected String getPresenceReporterKey()
    {
        return SametimePresenceReporter.KEY;
    }

    protected SametimePresenceReporter createPresenceReporter()
    {
        return new SametimePresenceReporter(localeSupport, bandanaManager, velocityHelperService);
    }

    @Test
    public void testGetKey()
    {
        assertEquals("sametime", createPresenceReporter().getKey());
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.sametime.name", createPresenceReporter().getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.sametime.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertEquals(true, createPresenceReporter().hasConfig());
    }

    @Test
    public void testGetPresenceXHTMLWithoutSpecifyingId() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, true);

        assertNotNull(velocityContext);
        assertTrue(velocityContext.containsKey("user"));
        assertNull(velocityContext.get("user"));
    }

    @Test
    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, true);
        assertNotNull(velocityContext);
        assertEquals(Boolean.TRUE, velocityContext.get("outputId"));
    }

    @Test
    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, false);
        assertNotNull(velocityContext);
        assertEquals(Boolean.FALSE, velocityContext.get("outputId"));
    }
}