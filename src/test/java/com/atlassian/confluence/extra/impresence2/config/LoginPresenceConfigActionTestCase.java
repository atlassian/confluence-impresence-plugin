package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.LoginPresenceReporter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public abstract class LoginPresenceConfigActionTestCase<A extends LoginPresenceConfigAction, T extends LoginPresenceReporter> extends AbstractIMConfigActionTestCase<A, T>
{
    protected T createReporter()
    {
        T presenceReporter = mock(getPresenceReporterClass());
        when(presenceReporter.getKey()).thenReturn(getServiceKey());

        return presenceReporter;
    }

    public void testExecuteWhenUserIdNotSpecified() throws Exception
    {
        action.setReporterId(null);

        assertEquals("success", action.execute());

        verify(presenceReporter).setId(null);
    }

    public void testExecuteWhenOnlyUserIdSpecified() throws Exception
    {
        action.setReporterId("newFakeUserName");

        assertEquals("success", action.execute());

        verify(presenceReporter).setId("newFakeUserName");
        verify(presenceReporter).setPassword(null);
    }

    public void testExecuteWhenOnlyPasswordIsSpecified() throws Exception
    {
        action.setReporterPassword("newFakePassword");

        assertEquals("success", action.execute());

        verify(presenceReporter).setId(null);
        verify(presenceReporter).setPassword("newFakePassword");
    }

    protected abstract Class<T> getPresenceReporterClass();
}
