package com.atlassian.confluence.extra.impresence2.reporter;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSkypePresenceReporter extends AbstractPresenceReporterTest<SkypePresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return SkypePresenceReporter.KEY;
    }

    protected SkypePresenceReporter createPresenceReporter()
    {
        return new SkypePresenceReporter(localeSupport);
    }

    @Test
    public void testGetKey()
    {
        assertEquals("skype", createPresenceReporter().getKey());
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.skype.name", createPresenceReporter().getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.skype.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertEquals(false, createPresenceReporter().hasConfig());
    }

    @Test
    public void testRequiresConfig()
    {
        assertEquals(false, createPresenceReporter().requiresConfig());
    }

    @Test
    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(createPresenceReporter().getPresenceXHTML(null, false).indexOf("presencereporter.skype.error.noscreenname") >= 0);
    }

    @Test
    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer out = new StringBuffer();

        out.append("<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script>")
                .append("<a href='skype:").append(id).append("?call'>")
                .append("<img src='http://mystatus.skype.com/smallclassic/").append(id).append("' style='border: none;' align='absmiddle' alt='My status' />")
                .append("</a>");

        out.append("&nbsp;<a href='skype:").append(id).append("?call'>")
                .append(id)
                .append("</a>");

        assertEquals(
                out.toString(),
                createPresenceReporter().getPresenceXHTML(id, true));
    }

    @Test
    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer out = new StringBuffer();


        out.append("<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script>")
                .append("<a href='skype:").append(id).append("?call'>")
                .append("<img src='http://mystatus.skype.com/smallclassic/").append(id).append("' style='border: none;' align='absmiddle' alt='My status' />")
                .append("</a>");

        assertEquals(
                out.toString(),
                createPresenceReporter().getPresenceXHTML(id, false));
    }
}