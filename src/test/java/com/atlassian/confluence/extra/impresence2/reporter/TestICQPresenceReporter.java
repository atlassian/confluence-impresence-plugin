package com.atlassian.confluence.extra.impresence2.reporter;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestICQPresenceReporter extends AbstractPresenceReporterTest<ICQPresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return ICQPresenceReporter.KEY;
    }

    protected ICQPresenceReporter createPresenceReporter()
    {
        return new ICQPresenceReporter(localeSupport);
    }

    @Test
    public void testGetKey()
    {
        assertEquals("icq", ICQPresenceReporter.KEY);
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.icq.name", presenceReporter.getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.icq.servicehomepage", presenceReporter.getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertEquals(false, presenceReporter.hasConfig());
    }

    @Test
    public void testRequiresConfig()
    {
        assertEquals(false, presenceReporter.requiresConfig());
    }

    @Test
    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(presenceReporter.getPresenceXHTML(null, false).indexOf("presencereporter.icq.error.nouin") >= 0);
    }

    @Test
    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a title=\"ICQ " + id + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\"><img border=\"0\" align=\"absmiddle\""
                        + " src=\"http://status.icq.com/online.gif?icq="
                        + id
                        + "&img=5\"></a>"
                        + "&nbsp<a title=\"ICQ "
                        + id
                        + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\">"
                        + id
                        + "</a>",
                presenceReporter.getPresenceXHTML(id, true));
    }

    @Test
    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a title=\"ICQ " + id + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\"><img border=\"0\" align=\"absmiddle\" "
                        + "src=\"http://status.icq.com/online.gif?icq="
                        + id
                        + "&img=5\"></a>",
                presenceReporter.getPresenceXHTML(id, false));
    }
}