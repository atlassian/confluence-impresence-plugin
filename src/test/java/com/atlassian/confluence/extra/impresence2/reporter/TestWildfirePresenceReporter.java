package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestWildfirePresenceReporter extends AbstractServerPresenceReporterTest<WildfirePresenceReporter>
{

    protected String getPresenceReporterKey()
    {
        return WildfirePresenceReporter.KEY;
    }

    protected WildfirePresenceReporter createPresenceReporter()
    {
        return new WildfirePresenceReporter(localeSupport, bandanaManager);
    }

    @Test
    public void testGetKey()
    {
        assertEquals("wildfire", createPresenceReporter().getKey());
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.wildfire.name", createPresenceReporter().getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.wildfire.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertEquals(true, createPresenceReporter().hasConfig());
    }

    @Test
    public void testGetPresenceXHTMLWithoutSpecifyingId() throws IOException, PresenceException
    {
        assertTrue(createPresenceReporter().getPresenceXHTML(null, true).indexOf("presencereporter.wildfire.error.noscreename") >= 0);
    }

    @Test
    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer expectedOutputStringBuffer = new StringBuffer();

        expectedOutputStringBuffer.append("<a href='jabber:").append(id).append("'")
                .append(" title='Online status for ").append(id).append("'")
                .append(" style='white-space:nowrap;'>");

        expectedOutputStringBuffer.append("<img src='http://").append("imaginary-server").append("/plugins/presence/status?jid=").append(id).append("'")
                .append(" align='absmiddle' border='0' title='Status Indicator' alt='Status Indicator'/>");

        expectedOutputStringBuffer.append("</a>");

        expectedOutputStringBuffer.append("&nbsp;<a href='jabber:").append(id).append("'")
                .append(" title='Online status for ").append(id).append("'")
                .append(" style='white-space:nowrap;'>").append(id).append("</a>");

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                SERVER_NAME + getPresenceReporterKey()
        )).thenReturn("imaginary-server");

        assertEquals(
                expectedOutputStringBuffer.toString(),
                createPresenceReporter().getPresenceXHTML(id, true));
    }

    @Test
    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer expectedOutputStringBuffer = new StringBuffer();

        expectedOutputStringBuffer.append("<a href='jabber:").append(id).append("'")
                .append(" title='Online status for ").append(id).append("'")
                .append(" style='white-space:nowrap;'>");

        expectedOutputStringBuffer.append("<img src='http://").append("imaginary-server").append("/plugins/presence/status?jid=").append(id).append("'")
                .append(" align='absmiddle' border='0' title='Status Indicator' alt='Status Indicator'/>");

        expectedOutputStringBuffer.append("</a>");

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                SERVER_NAME + getPresenceReporterKey()
        )).thenReturn("imaginary-server");

        assertEquals(
                expectedOutputStringBuffer.toString(),
                createPresenceReporter().getPresenceXHTML(id, false));
    }
}