package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestJabberPresenceReporter extends AbstractLoginPresenceReporterTest<JabberPresenceReporter>
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule().silent();
    @Mock
    private XMPPTCPConnection xmppConnection;
    @Mock
    private Roster roster;
    @Mock
    private RosterFactory rosterFactory;


    @Before
    public void setUp()
    {
        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getPresenceReporterKey())).thenReturn("john.doe");
        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, PASSWORD_PREFIX + getPresenceReporterKey())).thenReturn("secret");

        when(xmppConnection.isConnected()).thenReturn(true);
        when(xmppConnection.isAuthenticated()).thenReturn(true);
        when(rosterFactory.createRoster(xmppConnection)).thenReturn(roster);
    }

    protected JabberPresenceReporter createPresenceReporter()
    {
        return new JabberPresenceReporter(localeSupport, bandanaManager, bootstrapManager, rosterFactory)
        {
            @Override
            public XMPPTCPConnection createXmppConnection(String domainName)
            {
                return xmppConnection;
            }
        };
    }

    protected String getPresenceReporterKey()
    {
        return JabberPresenceReporter.KEY;
    }

    @Test
    public void testGetKey()
    {
        assertEquals("jabber", presenceReporter.getKey());
    }

    @Test
    public void testGetName()
    {
        assertEquals("presencereporter.jabber.name", presenceReporter.getName());
    }

    @Test
    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.jabber.servicehomepage", presenceReporter.getServiceHomepage());
    }

    @Test
    public void testHasConfig()
    {
        assertTrue(createPresenceReporter().hasConfig());
    }

    @Test
    public void testGetPresenceXhtmlWhenAddingBuddy() throws IOException, PresenceException, XMPPException
    {
        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presencereporter.jabber.message.waitinbuddyaccept") >= 0);
    }

    @Test
    public void testGetPresenceXhtmlWhenWaitingForBuddyUnblock() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(JidCreate.bareFrom("jane.doe"))).thenReturn(true);
        when(roster.getEntry(JidCreate.bareFrom("jane.doe"))).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");
        when(aRosterEntry.isSubscriptionPending()).thenReturn(true);
        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presence.link.waitingunblock") >= 0);
    }

    @Test
    public void testGetPresenceXhtmlWhenPresenceCouldNotBeObtained() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(JidCreate.bareFrom("jane.doe"))).thenReturn(true);
        when(roster.getEntry(JidCreate.bareFrom("jane.doe"))).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presence.link.intermediate") >= 0);
    }

    @Test
    public void testGetPresenceXhtmlWhenPresenceCouldBeObtained() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(JidCreate.bareFrom("jane.doe"))).thenReturn(true);
        when(roster.getEntry(JidCreate.bareFrom("jane.doe"))).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        Presence presence = new Presence(Presence.Type.available);
        presence.setMode(Presence.Mode.away);
        when(roster.getPresence(JidCreate.bareFrom("jane.doe"))).thenReturn(presence);

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("away") >= 0);
    }

    @Test
    public void testGetPresenceXhtmlWhenPresenceModeIsNullAndPresenceTypeIsAvailable() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(JidCreate.bareFrom("jane.doe"))).thenReturn(true);
        when(roster.getEntry(JidCreate.bareFrom("jane.doe"))).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        Presence presence = new Presence(Presence.Type.available);
        when(roster.getPresence(JidCreate.bareFrom("jane.doe"))).thenReturn(presence);

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("im_free_chat.gif") >= 0);
    }

    @Test
    public void testXmppConnectionCreatedWithTheDomainOfTheConfiguredDummyUser() throws IOException, PresenceException
    {
        final String customDomain = "foo.bar.baz";

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getPresenceReporterKey())).thenReturn("john.doe@" + customDomain);

        presenceReporter = new JabberPresenceReporter(localeSupport, bandanaManager, bootstrapManager, rosterFactory)
        {
            @Override
            XMPPTCPConnection createXmppConnection(String domainName)
            {
                if (!StringUtils.equals(customDomain, domainName))
                    fail("XMPP connection not created with domain " + customDomain);
                return xmppConnection;
            }
        };

        assertNotNull(presenceReporter.getPresenceXHTML("jane.doe", true));
    }

    @Test
    public void testGetPresenceXhtmlWhenNotAuthenticatedToJabber() throws IOException, PresenceException, XMPPException
    {
        when(xmppConnection.isAuthenticated()).thenReturn(false);

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presencereporter.jabber.error.login") >= 0);
    }
}
