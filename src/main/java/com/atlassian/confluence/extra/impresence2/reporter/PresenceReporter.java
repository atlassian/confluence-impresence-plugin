/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.reporter;

import java.io.IOException;

/**
 * TODO: Document this class.
 * <p>
 * User: david
 * Date: Jun 19, 2006
 * Time: 4:10:05 PM
 */
public interface PresenceReporter
{
    /**
     * Returns the key that will be used to identify the reporter.
     * @return The reporter key.
     */
    public String getKey();

    /**
     * Returns the human-friendly name of the reporter. Eg 'MSN' or 'AIM'.
     * @return The name of the reporter.
     */
    public String getName();

    /**
     * Returns the URL of the homepage for the service, where people can sign up.
     *
     * @return The URL.
     */
    public String getServiceHomepage();

    /**
     * @return <code>true</code> if the presence reporter can be configured.
     */
    public boolean hasConfig();

    /**
     * Returns <code>true</code> if the reporter requires configuration.
     *
     * @return <code>false</code> if the reporter is ready.
     */
    public boolean requiresConfig();

    /**
     * Returns the XHTML displaying the presence status of the specified user id.
     *
     * @param id The username to check.
     * @param outputId If true, output the ID as well as the icon.
     * @return The presence status for the user.
     * @throws java.io.IOException ioException
     * @throws PresenceException presenceException
     */
    public String getPresenceXHTML(String id, boolean outputId) throws IOException, PresenceException;
}
