package com.atlassian.confluence.extra.impresence2;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ModuleType(ListableModuleDescriptorFactory.class)
public class PresenceReporterModuleDescriptorFactory extends SingleModuleDescriptorFactory<PresenceReporterModuleDescriptor> {

    @Autowired
    public PresenceReporterModuleDescriptorFactory(HostContainer hostContainer) {
        super(hostContainer, "presence-reporter", PresenceReporterModuleDescriptor.class);
    }
}
