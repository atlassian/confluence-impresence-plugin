package com.atlassian.confluence.extra.impresence2.reporter;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.roster.Roster;
import org.springframework.stereotype.Component;

/**
 * Class just to avoid using powermock and enable stubbing.
 * @since 4.0.4
 */
@Component
class RosterFactory {
    public Roster createRoster(XMPPConnection connection) {
        return Roster.getInstanceFor(connection);
    }
}
