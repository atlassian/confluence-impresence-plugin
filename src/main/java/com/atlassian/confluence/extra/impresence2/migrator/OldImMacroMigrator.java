package com.atlassian.confluence.extra.impresence2.migrator;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.MacroBody;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.renderer.v2.macro.Macro;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;

/**
 * Migrates old IM macros (e.g {@code {skypeme, skype, wildfire}}
 */
public class OldImMacroMigrator implements MacroMigration
{
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        MacroBody macroBody = macroDefinition.getBody();
        final String macroBodyText = macroBody.getBody();

        if (StringUtils.isNotBlank(macroBodyText))
        {
            macroDefinition.setDefaultParameterValue(macroBodyText);
            macroDefinition.setBody(new PlainTextMacroBody(""));
        }

        return macroDefinition;
    }
}
